<?php
 require_once dirname(__FILE__)."/../src/book.php";
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase {

    private $page = 0;
    private $book = null;

    public function setup() {
        $this->page = 1;
        $this->book = new \src\Book();
    }    

    public function testClassHasAttribute() {
        $this->assertClassHasAttribute('page', \src\Book::class);
    }

    public function testOpen() {
        $this->assertEquals(null, $this->book->open());
    }

    public function testTurnPage() {
        $this->assertEquals($this->page+1, $this->book->turnPage());
    }

    public function testGetPage() {
        $this->assertEquals($this->page, $this->book->getPage($this->page));
    }

}
