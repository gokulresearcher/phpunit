<?php
require_once dirname(__FILE__).'/../src/book.php';
require_once dirname(__FILE__).'/../src/kindle.php';
use PHPUnit\Framework\TestCase;

class EBookAdapterTest extends TestCase {

    private $book = null;
    private $kindle = null;

    public function setup() {
       $this->book = new \src\Book();
       $this->kindle = new \src\Kindle();
    }

    public function testCanTurnPageInBook() {
        $this->book->open();
        $page = $this->book->turnPage();

        $this->assertEquals(2, $page);
    }

    public function testCanTurnPageInKindleSameAsBook() {
        $this->kindle->unlock();
        $page = $this->kindle->nextPage();

        $this->assertEquals($this->book->turnPage(), $page);
    }

}