<?php
require_once dirname(__FILE__).'/../src/kindle.php';
use PHPUnit\Framework\TestCase;

class KindleTest extends TestCase {

    private $page = 0;
    private $kindle = null;

    public function setup() {
        $this->page = 1;
        $this->kindle = new \src\Kindle();
    } 

    public function testClassHasAttribute() {
        $this->assertClassHasAttribute('page', \src\Kindle::class);
    }

    public function testUnlock() {
        $this->assertEquals($this->page, $this->kindle->unlock());
    }

    public function testNextPage() {
        $this->assertEquals($this->page+1, $this->kindle->nextPage());
    }

    public function testNavigatePage() {
        $this->assertEquals($this->page, $this->kindle->navigatePage($this->page));
    }

}
