In this example will demonstrate the value of unit-testing. To explain the scenario we can 
pick adapter design pattern and write unit-testing for the same.

You can now run the ant clean && ant build command to see the result (which is success).

Now to see the value of unit-testing and how the bugs can be prevented. You as a developer you 
can modify the turnPage functionality in src/book.php 
For instance you can try 

    Actual logic : 
    public function turnPage() {
        $this->page += 1;
        return $this->page;
    }

    change it as :    
    public function turnPage() {
        $this->page += 2;
        return $this->page;
    }

After making changes to functionality, run the ant build command. The result of which is 
failure. Which gives developer a confidence that something went wrong and can be fixed, 
even before sending it to peer-review. Thus how unit-testing helps to prevent bugs.
