<?php
namespace src;
require_once("ibook.php");
require_once("iebook.php");
class EBookAdapter implements IBook {

    protected $eBook = null;

    public function __construct(EBookInterface $ebook){
        $this->eBook = $ebook;
    }

    public function open() {
        $this->eBook->unlock();
    }

    public function turnPage() {
        $this->eBook->nextPage();
    }

    public function getPage($page) {
        $this->eBook->navigatePage($page);
    }

}
