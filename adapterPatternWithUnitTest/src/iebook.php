<?php
namespace src;
interface EBookInterface {
    public function unlock();
    public function nextPage();
    public function navigatePage($page);
}
