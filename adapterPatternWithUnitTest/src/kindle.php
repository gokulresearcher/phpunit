<?php
namespace src;
require_once('iebook.php');

class Kindle implements EBookInterface {

    protected $page = 1;

    public function __construct() {
        $this->unlock();
    }

    public function unlock() {
        return $this->page;
    }

    public function nextPage() {
        $this->page += 1;
        return $this->page;
    }

    public function navigatePage($page) {
        $this->page = $page;
        return $this->page;
    }

}
