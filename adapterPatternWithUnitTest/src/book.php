<?php
namespace src;
require_once('ibook.php');
class Book implements IBook {

    protected $page = 1;

    public function __construct(){
        $this->open();
    }

    public function open() {
        $this->page = 1;
    }

    public function turnPage() {
        $this->page += 1;
        return $this->page;
    }

    public function getPage($page) {
        $this->page = $page;
        return $this->page;
    }

}
