<?php
namespace src;
interface IBook {
    public function open();
    public function turnPage();
    public function getPage($page);
}
