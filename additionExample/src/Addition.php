<?php
namespace src;
class Addition {

    public function __construct(){

    }

    public function addTwoNumbers($a, $b) {
        return $a + $b;
    }

    public function addThreeNumbers($a, $b, $c) {
        return $a + $b + $c;
    }

    public function addNNumbers($numbers) {
        $result = 0;

        foreach ($numbers as $number)
            $result += $number;

        return $result;
    }
}
