<?php
 require_once dirname(__FILE__)."/../src/Addition.php";

use PHPUnit\Framework\TestCase;
class AdditionTest extends TestCase {

    public function twoNumberAdditionProvider() {
        return [
            [1, 0, 0],
            [0, 1, 1],
            [2, 0, 1],
            [2, 2, 4]
        ];
    }

    /**
    * @dataProvider twoNumberAdditionProvider
    */
    public function testAddTwoNumbers($a, $b, $expected) {
        $addition = new \src\Addition();
        $this->assertEquals($expected, $addition->addTwoNumbers($a, $b));
    }
}
