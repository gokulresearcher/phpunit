# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains code-snippet for demonstrating unit-testing. I will be using the following verion
  phpunit 6.3(stable)
* You can find more documentation on phpunit here.(https://phpunit.de/manual)

### How do I get set up? ###

* Download the ".phar" from phpunit website . Its recommended to use stable version. In our sample we are using 6.3
* Summary of set up (run the following commands)
 -chmod +x phpunit.phar
 -sudo mv phpunit.phar /usr/local/bin/phpunit
 -phpunit --version